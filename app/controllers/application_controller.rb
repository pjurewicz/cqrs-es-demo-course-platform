class ApplicationController < ActionController::Base
  add_breadcrumb 'courses', :root_path

  def event_store
    Rails.configuration.event_store
  end

  def command_bus
    Rails.configuration.command_bus
  end
end
