class CoursesController < ApplicationController
  def index
    @courses = Course.all
  end

  def new
    @create_course_cmd = CourseManagement::DraftCourse.new(course_uuid: SecureRandom.uuid)
  end

  def create
    @create_course_cmd = CourseManagement::DraftCourse.new(course_params)
    command_bus.(@create_course_cmd)
    if @create_course_cmd.errors.any?
      render 'new'
    else
      redirect_to courses_path, notice: 'Course was successfully created.'
    end
  end

  private

  def course_params
    params.require(:course).permit(:course_uuid, :name, :description)
  end
end