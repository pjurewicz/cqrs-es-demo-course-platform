class LessonsController < ApplicationController
  before_action :set_course, except: :show
  before_action :set_lesson, only: [:show, :add_downloadable_material, :add_downloadable_material]

  def index
    add_breadcrumb @course.name if @course
    add_breadcrumb 'lessons', course_lessons_path(@course)
    @lessons = Lesson.where(course_uuid: params[:course_uuid])
  end

  def show
    add_breadcrumb @lesson.course.name if @lesson.course
    add_breadcrumb 'lessons', course_lessons_path(@lesson.course)
    add_breadcrumb @lesson.title, lesson_path(@lesson)
    @add_material_command = CourseManagement::AddDownloadableMaterialToLesson.new
  end

  def new
    add_breadcrumb @course.name if @course
    add_breadcrumb 'lessons', course_lessons_path(@course)
    add_breadcrumb 'new lesson', new_course_lesson_path(@course)
    @create_lesson_cmd = CourseManagement::DraftLesson.new(lesson_uuid: SecureRandom.uuid)
  end

  def create
    @create_lesson_cmd = CourseManagement::DraftLesson.new(lesson_params)
    @create_lesson_cmd.course = @course
    command_bus.(@create_lesson_cmd)
    if @create_lesson_cmd.errors.any?
      render 'new'
    else
      redirect_to course_lessons_path(@course), notice: 'Lesson was successfully created.'
    end
  end

  def add_downloadable_material
    @add_material_command = CourseManagement::AddDownloadableMaterialToLesson.new(add_downloadable_material_params)
    @add_material_command.lesson_uuid = @lesson.uuid
    command_bus.(@add_material_command)
    if @add_material_command.errors.any?
      render 'show'
    else
      redirect_to lesson_path(@lesson), notice: 'Material was successfully added to lesson.'
    end
  rescue CourseManagement::Lesson::IncorrectAttachmentToken
    flash.now[:alert] = 'There was a problem with file submission.'
    render 'show'
  end

  private

  def set_lesson
    @lesson = Lesson.find(params[:id])
  end

  def set_course
    @course = Course.find(params[:course_uuid]) if params[:course_uuid].present?
  end

  def lesson_params
    params.require(:lesson).permit(:lesson_uuid, :title, :description)
  end

  def add_downloadable_material_params
    params.require(:downloadable_material).permit(:attachment_name, :attachment)
  end
end