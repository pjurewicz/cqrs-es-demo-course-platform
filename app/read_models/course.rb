class Course < ApplicationRecord
  has_many :lessons, foreign_key: :course_uuid
end