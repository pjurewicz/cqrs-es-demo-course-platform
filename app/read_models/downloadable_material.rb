class DownloadableMaterial < ApplicationRecord
  belongs_to :lesson, foreign_key: :lesson_uuid
  has_one_attached :attachment
end