class Lesson < ApplicationRecord
  belongs_to :course, foreign_key: :course_uuid, optional: true
  has_many :downloadable_materials, foreign_key: :lesson_uuid
end