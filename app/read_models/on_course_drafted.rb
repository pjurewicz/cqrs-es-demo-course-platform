class OnCourseDrafted
  def call(event)
    Course.create!(uuid: event.data[:course_uuid], name: event.data[:name], description: event.data[:description])
  end
end