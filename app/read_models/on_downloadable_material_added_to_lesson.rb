class OnDownloadableMaterialAddedToLesson
  def call(event)
    lesson = Lesson.find(event.data[:lesson_uuid])
    DownloadableMaterial.create!(lesson: lesson, attachment: event.data[:attachment_token], attachment_name: event.data[:attachment_name])
  end
end
