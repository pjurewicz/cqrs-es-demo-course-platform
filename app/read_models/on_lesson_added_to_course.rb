class OnLessonAddedToCourse
  def call(event)
    course = Course.find(event.data[:course_uuid])
    course.with_lock do
      course.lessons_count += 1
      course.save!
    end
  end
end