class OnLessonDrafted
  def call(event)
    Lesson.create!(
        uuid: event.data[:lesson_uuid],
        title: event.data[:title],
        description: event.data[:description],
        course_uuid: event.data[:course_uuid],
        course_name: event.data[:course_name],
    )
  end
end