Rails.application.routes.draw do
  mount RailsEventStore::Browser => '/res'

  root to: redirect('courses')

  resources :courses, only: [:new, :create, :index]

  get '(courses/:course_uuid)/lessons', to: 'lessons#index', as: 'course_lessons'
  get '(courses/:course_uuid)/lessons/new', to: 'lessons#new', as: 'new_course_lesson'
  post '(courses/:course_uuid)/lessons', to: 'lessons#create'

  resources :lessons, only: [:show] do
    put 'add_downloadable_material', on: :member
  end
end
