require 'aggregate_root'

module CourseManagement
  class Course
    include AggregateRoot

    def initialize uuid
      @uuid = uuid
      @lesson_uuids = []
    end

    def draft name, description
      apply CourseDrafted.new(data: {course_uuid: @uuid, name: name, description: description})
    end

    def add_lesson lesson_uuid
      apply LessonAddedToCourse.new(data: {course_uuid: @uuid, lesson_uuid: lesson_uuid})
    end

    private

    on CourseDrafted do |event|
      @state = :draft
      @name = event.data[:name]
      @description = event.data[:description]
    end

    on LessonAddedToCourse do |event|
      @lesson_uuids << event.data[:lesson_uuid]
    end
  end
end