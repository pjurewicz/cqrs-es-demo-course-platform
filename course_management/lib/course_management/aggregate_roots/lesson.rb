require 'aggregate_root'

module CourseManagement
  class Lesson
    include AggregateRoot
    IncorrectAttachmentToken = Class.new(StandardError)

    class DownloadableMaterial
      attr_accessor :attachment_name
      attr_accessor :attachment_token

      def initialize attachment_name, attachment_key
        @attachment_name = attachment_name
        @attachment_key = attachment_key
      end

      def blob
        ActiveStorage::Blob.find_by_key(@attachment_key)
      end
    end

    def initialize uuid
      @uuid = uuid
      @downloadable_materials = []
    end

    def draft title, description, course = nil
      data = {lesson_uuid: @uuid, title: title, description: description}
      if course
        data.merge!(course_uuid: course.uuid, course_name: course.name)
      end
      apply LessonDrafted.new(data: data)
    end

    def add_downloadable_material_to_course attachment_name, attachment_token
      begin
        blob = ActiveStorage::Blob.find_signed(attachment_token)
      rescue ActiveSupport::MessageVerifier::InvalidSignature
        raise IncorrectAttachmentToken.new
      end
      apply DownloadableMaterialAddedToLesson.new(data: {lesson_uuid: @uuid, attachment_name: attachment_name, attachment_token: attachment_token, attachment_key: blob.key})
    end

    private

    on LessonDrafted do |event|
      @title = event.data[:title]
      @description = event.data[:description]
      @course_uuid = event.data[:course_uuid]
    end

    on DownloadableMaterialAddedToLesson do |event|
      @downloadable_materials << DownloadableMaterial.new(event.data[:attachment_name], event.data[:attachment_key])
    end
  end
end