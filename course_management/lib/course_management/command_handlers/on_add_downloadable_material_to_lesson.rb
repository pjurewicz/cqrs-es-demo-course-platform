module CourseManagement
  class OnAddDownloadableMaterialToLesson
    include CommandHandler

    def call(command)
      return unless command.valid?
      ActiveRecord::Base.transaction do
        with_aggregate(Lesson, command.lesson_uuid) do |lesson|
          lesson.add_downloadable_material_to_course command.attachment_name, command.attachment
        end
      end
    end
  end
end