module CourseManagement
  class OnDraftCourse
    include CommandHandler

    def call(command)
      return unless command.valid?
      ActiveRecord::Base.transaction do
        with_aggregate(Course, command.course_uuid) do |course|
          course.draft command.name, command.description
        end
      end
    end
  end
end