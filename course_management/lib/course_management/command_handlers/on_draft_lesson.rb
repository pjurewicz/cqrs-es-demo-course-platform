module CourseManagement
  class OnDraftLesson
    include CommandHandler

    def call(command)
      return unless command.valid?
      ActiveRecord::Base.transaction do
        with_aggregate(Lesson, command.lesson_uuid) do |lesson|
          lesson.draft command.title, command.description, command.course
        end
        if command.course&.uuid
          with_aggregate(Course, command.course.uuid) do |course|
            course.add_lesson command.lesson_uuid
          end
        end
      end
    end
  end
end