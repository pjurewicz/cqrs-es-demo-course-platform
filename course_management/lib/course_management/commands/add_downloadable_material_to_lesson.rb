module CourseManagement
  class AddDownloadableMaterialToLesson < Command
    attribute :lesson_uuid, :string
    attribute :attachment_name, :string
    attribute :attachment, :string

    validates :attachment_name, :attachment, presence: true
  end
end