module CourseManagement
  class DraftCourse < Command
    attribute :course_uuid, :string
    attribute :name, :string
    attribute :description, :string

    validates :name, presence: true
  end
end