module CourseManagement
  class DraftLesson < Command
    attribute :lesson_uuid, :string
    attribute :title, :string
    attribute :description, :string

    attr_accessor :course

    validates :title, presence: true
  end
end