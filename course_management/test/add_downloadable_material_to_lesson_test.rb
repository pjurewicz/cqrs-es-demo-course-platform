require_relative 'test_helper'

module CourseManagement
  class AddDownloadableMaterialToLessonTest < ActiveSupport::TestCase
    include TestCase

    test 'not processed file could not be add to lesson' do
      aggregate_id = SecureRandom.uuid
      stream = "CourseManagement::Lesson$#{aggregate_id}"
      lesson_title = Faker::Educator.subject
      lesson_description = Faker::Lorem.paragraph
      arrange(stream, [
          LessonDrafted.new(data: {lesson_uuid: aggregate_id, title: lesson_title, description: lesson_description})
      ])

      assert_raises(Lesson::IncorrectAttachmentToken) do
        act(stream, AddDownloadableMaterialToLesson.new(lesson_uuid: aggregate_id, attachment_name: 'lorem ipsum', attachment: 'MALFORMED_TOKEN'))
      end
    end
  end
end