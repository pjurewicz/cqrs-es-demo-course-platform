class CreateCourse < ActiveRecord::Migration[5.2]
  def change
    create_table :courses, id: false do |t|
      t.string :uuid, primary_key: true
      t.string :name
      t.text :description
    end
  end
end