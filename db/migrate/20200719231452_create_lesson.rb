class CreateLesson < ActiveRecord::Migration[5.2]
  def change
    create_table :lessons , id: false do |t|
      t.string :uuid, primary_key: true
      t.string :title
      t.text :description
      t.string :course_uuid
      t.string :course_name
    end
  end
end
