class CreateDownloadableMaterial < ActiveRecord::Migration[5.2]
  def change
    create_table :downloadable_materials do |t|
      t.string :lesson_uuid
      t.string :attachment_name
    end
  end
end
