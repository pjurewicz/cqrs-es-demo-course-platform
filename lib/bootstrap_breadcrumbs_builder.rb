class BootstrapBreadcrumbsBuilder < BreadcrumbsOnRails::Breadcrumbs::Builder
  def render
    @context.content_tag(:ul, class: 'breadcrumb') do
      @elements.collect do |element|
        render_element(element)
      end.join.html_safe
    end
  end

  def render_element(element)
    @context.content_tag(:li, class: "breadcrumb-item") do
      if element.path.present?
        @context.link_to(compute_name(element), compute_path(element), element.options)
      else
        compute_name(element)
      end
    end
  end
end