require 'test_helper'

class CourseDraftedTest < ActiveJob::TestCase
  test 'draft new course' do
    event_store = Rails.configuration.event_store

    course_uuid = SecureRandom.uuid
    name = Faker::Educator.course_name
    description = Faker::Lorem.paragraph
    event_store.publish(CourseManagement::CourseDrafted.new(data: {course_uuid: course_uuid, name: name, description: description}))

    assert_equal(Course.count, 1)
  end
end
