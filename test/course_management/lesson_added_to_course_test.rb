require 'test_helper'

class LessonAddedToCourseTest < ActiveJob::TestCase
  test 'add lesson to course' do
    event_store = Rails.configuration.event_store

    course_uuid = SecureRandom.uuid
    course_name = Faker::Educator.course_name
    course_description = Faker::Lorem.paragraph
    course = Course.create!(uuid: course_uuid, name: course_name, description: course_description, )
    assert_equal(course.lessons_count, 0)

    lesson_uuid = SecureRandom.uuid
    lesson_title = Faker::Educator.subject
    lesson_description = Faker::Lorem.paragraph
    Lesson.create!(uuid: lesson_uuid, title: lesson_title, description: lesson_description, course_uuid: course_uuid)

    event_store.publish(CourseManagement::LessonAddedToCourse.new(data: {lesson_uuid: lesson_uuid, course_uuid: course_uuid}))
    course.reload
    assert_equal(course.lessons_count, 1)
  end
end