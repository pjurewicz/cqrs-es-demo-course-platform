require 'test_helper'

class LessonDraftedTest < ActiveJob::TestCase
  test 'draft new lesson' do
    event_store = Rails.configuration.event_store

    lesson_uuid = SecureRandom.uuid
    title = Faker::Educator.subject
    description = Faker::Lorem.paragraph
    event_store.publish(CourseManagement::LessonDrafted.new(data: {lesson_uuid: lesson_uuid, title: title, description: description}))

    assert_equal(Lesson.count, 1)
  end
end
