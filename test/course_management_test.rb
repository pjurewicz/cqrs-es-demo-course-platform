require 'test_helper'
path = Rails.root.join('course_management/test')

Dir.glob("#{path}/**/*_test.rb") do |file|
  require file
end
